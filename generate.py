from random import shuffle
import cv2

def concat_vh(list_2d):
      # return final image
    return cv2.vconcat([cv2.hconcat(list_h) 
                        for list_h in list_2d])

def openimage(i:int):
    return cv2.imread(f'helt_brett_{i}.png')

l = [i for i in range(25)]

for j in range(1000):
    shuffle(l)

    list2d = [[openimage(number) for number in l[5 * i : 5* i + 5]]  for i in range(5)]

    img_tile = concat_vh(list2d)

    cv2.imwrite(f"generert/brett{j}.png", img_tile)
